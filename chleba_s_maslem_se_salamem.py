def main():
    pastries = (
        # 1. pád      4. pád       5. pád
        {1: "chleba", 4: "chleba", 5: "chlebe"},
        {1: "rohlík", 4: "rohlík", 5: "rohlíku"},
        {1: "houska", 4: "housku", 5: "housko"},
        {1: "veka", 4: "veku", 5: "veko"},
        {1: "loupák", 4: "loupák", 5: "loupáku"},
        {1: "dalamánek", 4: "dalamánek", 5: "dalamánku"},
        {1: "mazanec", 4: "mazanec", 5: "mazanče"},
        {1: "vánočka", 4: "vánočku", 5: "vánočko"},
        {1: "pletýnka", 4: "pletýnku", 5: "pletýnko"},
        {1: "toast", 4: "toast", 5: "toaste"},
        # {1: "bageta", 4: "bagetu", 5: "bageto"},
        # {1: "bulka", 4: "bulku", 5: "bulko"},
        # {1: "preclík", 4: "preclík", 5: "preclíku"},
        # {1: "croissant", 4: "croissant", 5: "croissante"},
        # {1: "langoš", 4: "langoš", 5: "langoši"},
        # {1: "placka", 4: "placku", 5: "placko"},
        # {1: "žemle", 4: "žemli", 5: "žemle"},
        # {1: "loupák", 4: "loupák", 5: "loupáku"},
        {1: "rajče", 4: "rajče", 5: "rajče"}
    )

    extras = (
        "",
        " s máslem",
        " s máslem se salámem",
        # " s máslem se salámem se šunkou",
        # " s máslem se salámem se šunkou se sýrem",
        # " s máslem se salámem se šunkou se sýrem s rajčetem",
        # " s máslem se salámem se šunkou se sýrem s rajčetem s okurkou",
        # " s paštikou"
    )
    
    members = [
        pastries[0]
    ]

    def list_members(members, pad):
        return ", ".join([member[pad] for member in members])

    with open("output_one_line.txt", "w") as output_one_line, open("output_lines.txt", "w") as output_lines:
        first_loop = True
        for pastry in pastries:
            for extra in extras:
                if not any(member[1] == pastry[1] + extra for member in members):
                    text = f"{'Jde' if first_loop else 'Takže jde'} "
                    text += list_members(members, 1)
                    text += f" a potká {pastry[4] + extra}. A {pastry[1] + extra} povídá: \""
                    text += list_members(members, 5).capitalize()
                    text += f", můžu jet s {'tebou' if len(members) == 1 else 'váma'}?\" Přičemž "
                    text += list_members(members, 1)
                    text +=  f" odpoví: \"{'Ne, čas na smích' if pastry == pastries[-1] else 'Jo, můžeš'}.\".{' ' if pastry != pastries[-1] else ''}"

                    print(text)
                    output_one_line.write(text)
                    output_lines.write(text + "\n")

                    if pastry == pastries[-1]:
                        break

                    members.append(dict((key, value + extra) for key, value in pastry.items()))

                    if first_loop:
                        first_loop = False

if __name__ == "__main__":
    main()
